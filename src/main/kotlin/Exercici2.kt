
class Beguda(val nom: String, var preu: Double, val ensucrada: Boolean){
    init {
        if (ensucrada){
            preu += (preu*10)/100
        }
    }
}

fun main() {
    val croissant = Pasta("Croissant", 1.94, 430, 403)
    val ensaimada = Pasta("Ensaimada", 1.71, 350, 454)
    val donut = Pasta("Donut", 1.35, 240, 410)
    val aigua = Beguda("Aigua", 1.00, false)
    val cafe = Beguda("Café", 1.35, false)
    val teVermell = Beguda("Té vermell", 1.50, false)
    val cola = Beguda("Cola", 1.65, true)

    println("${croissant.tipus} ${croissant.preu}€ ${croissant.calorie}Kcal")
    println("${ensaimada.tipus} ${ensaimada.preu}€ ${ensaimada.calorie}Kcal")
    println("${donut.tipus} ${donut.preu}€ ${donut.calorie}Kcal")
    println("${aigua.nom} ${aigua.preu}€ ensucrada: ${aigua.ensucrada}")
    println("${cafe.nom} ${cafe.preu}€ ensucrada: ${cafe.ensucrada}")
    println("${teVermell.nom} ${teVermell.preu}€ ensucrada: ${teVermell.ensucrada}")
    println("${cola.nom} ${cola.preu}€ ensucrada: ${cola.ensucrada}")
}
