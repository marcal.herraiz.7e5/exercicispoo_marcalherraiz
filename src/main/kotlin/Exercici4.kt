const val RESET = "\u001B[0m"
const val BLACK = "\u001B[40m"
const val RED = "\u001B[41m"
const val GREEN = "\u001B[42m"
const val YELLOW = "\u001B[43m"
const val BLUE = "\u001B[44m"
const val PURPLE = "\u001B[45m"
const val CYAN = "\u001B[46m"
const val WHITE = "\u001B[47m"
val colors = arrayOf(BLACK, WHITE, RED, GREEN, YELLOW, BLUE, PURPLE, CYAN)

class Lampada(val id: String, private var on_Off: Boolean, var color: Int, var intensitat: Int){
    private var colorPositionGuardat = 1
    private var intensitatGuardada = 1

    fun changeState(){
       on_Off = !on_Off
        if (on_Off){
            color = colorPositionGuardat
            intensitat = 1
            intensitatGuardada = 1
        }else{
            color = 0
            intensitat = 0
        }
    }
    fun changeColor(){
        if (on_Off){
            if (color < colors.lastIndex) color++
            else color= 1
            colorPositionGuardat = color
        }
    }
    fun changeIntensity(){
        if (on_Off){
            if (intensitatGuardada<5){
                intensitatGuardada++
            }else intensitatGuardada = 1
            intensitat = intensitatGuardada
        }
    }
}

fun main(){
    val lampadaMenjadorInstruction = listOf("TURN ON", "CHANGE COLOR", "CHANGE COLOR", "CHANGE COLOR", "INTENSITY", "INTENSITY", "INTENSITY", "INTENSITY")
    val lampadaMenjador = Lampada("Menjador", false, 0, 0)
    val lampadaCuinaInstruction = listOf("TURN ON", "CHANGE COLOR", "CHANGE COLOR", "INTENSITY", "INTENSITY", "INTENSITY", "INTENSITY", "TURN OFF", "CHANGE COLOR", "TURN ON", "CHANGE COLOR", "INTENSITY", "INTENSITY", "INTENSITY", "INTENSITY")
    val lampadaCuina = Lampada("Cuina", false, 0, 0)

   lampadaSequence(lampadaMenjadorInstruction,lampadaMenjador)
   lampadaSequence(lampadaCuinaInstruction,lampadaCuina)
}

fun lampadaSequence(lampadaInstruction: List<String>, lampada: Lampada) {
    for (instr in lampadaInstruction){
        when(instr) {
            "TURN ON" -> { lampada.changeState() }
            "TURN OFF" -> { lampada.changeState()}
            "CHANGE COLOR" -> { lampada.changeColor() }
            "INTENSITY" -> { lampada.changeIntensity() }
        }
        println("${lampada.id} - Color: ${colors[lampada.color]}   $RESET - intensitat ${lampada.intensitat}")
    }
}

