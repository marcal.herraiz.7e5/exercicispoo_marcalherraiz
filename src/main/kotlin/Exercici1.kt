
data class Pasta(val tipus: String, val preu: Double, val pes: Int, val calorie: Int)

fun main() {
    val croissant = Pasta("Croissant", 1.94, 430, 403)
    val ensaimada = Pasta("Ensaimada", 1.71, 350, 454)
    val donut = Pasta("Donut", 1.35, 240, 410)
    println(croissant)
    println(ensaimada)
    println(donut)
}
