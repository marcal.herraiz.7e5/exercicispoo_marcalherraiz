import java.util.Scanner

class Taulell(private val preuUnitari: Int, private val llargada: Int, private val amplada: Int){
    init {
        costTotal += preuUnitari * (llargada*amplada)
    }
}
class Llisto(private val preuUnitari: Int, private val llargada: Int){
    init {
        costTotal += preuUnitari * llargada
    }
}

var costTotal = 0

fun main() {
    val sc = Scanner(System.`in`)
    println("Nombre d'elements:")
    val n_elements = sc.nextInt()

    for (i in 1..n_elements){
        println("Taulell o llistó?")
        val fusta = sc.next()
        if (fusta.uppercase()=="TAULELL"){
            val preuUnitari = sc.nextInt()
            val llargada = sc.nextInt()
            val amplada = sc.nextInt()
            Taulell(preuUnitari,llargada,amplada)
        }else if (fusta.uppercase()=="LLISTO"||fusta.uppercase()=="LLISTÓ"){
            val preuUnitari = sc.nextInt()
            val llargada = sc.nextInt()
            Llisto(preuUnitari,llargada)
        }
    }
    println("El preu total és de $costTotal€")
}