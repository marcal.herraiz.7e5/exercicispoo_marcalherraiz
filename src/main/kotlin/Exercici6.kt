
open class Electrodomestic(var preuBase: Int, val color: String= "blanc", val consum: String = "G", val pes: Int= 5){
    var preuFinal = 0
    init {
        this.preuFinal = this.preuBase + preuFinal()
    }
    fun preuFinal(): Int {
        var preuConsum = 0
        var preuPes = 0
        when(consum){
            "A" -> preuConsum+=35
            "B" -> preuConsum+=30
            "C" -> preuConsum+=25
            "D" -> preuConsum+=20
            "E" -> preuConsum+=15
            "F" -> preuConsum+=10
        }
        if (this.pes >=81){
            this.preuBase +=100
        }else{
            when(this.pes){
                in 6..20 -> preuPes+=20
                in 21..50 -> preuPes+=50
                in 51..80 -> preuPes+=80
            }
        }
        return preuConsum + preuPes
    }
}
class Rentadora(preuBase: Int, color: String, consum: String, pes: Int, private val carrega:Int= 5): Electrodomestic(preuBase,color, consum, pes){
    init {
        val preuCarrega = preuFinalCarrega()
        this.preuFinal += preuCarrega
    }
    private fun preuFinalCarrega(): Int{
        var preuCarrega = 0
        when(this.carrega){
            6,7 -> preuCarrega+= 55
            8 -> preuCarrega+= 70
            9 -> preuCarrega+= 85
            10 -> preuCarrega+= 100
        }
        return preuCarrega
    }
}
class Televisio(preuBase: Int, color: String, consum: String, pes: Int, private val tamany:Int= 28): Electrodomestic(preuBase,color, consum, pes){
    init {
        val preuTamany = preuFinalTamany()
        this.preuFinal += preuTamany
    }
    private fun preuFinalTamany(): Int {
        var preuTamany = 0
        if (this.tamany >=51){
            preuTamany+=200
        }else{
            when(this.tamany){
                in 29..32 -> preuTamany+=50
                in 33..42 -> preuTamany+=100
                in 43..50 -> preuTamany+=150
            }
        }
        return preuTamany
    }
}
fun main() {
    val llistaElectro = listOf(
        Electrodomestic(35,"blanc", "D", 2), Electrodomestic(40,"platejat", "B", 26),
        Electrodomestic(25,"platejat", "C", 58), Electrodomestic(36,"blanc", "D", 7),
        Electrodomestic(15,"blanc", "E", 49), Electrodomestic(70,"platejat", "F", 79),
        Rentadora(50,"platejat", "G", 18, 7), Rentadora(45,"blanc", "A", 32, 10),
        Televisio(150,"blanc", "B", 61, 30), Televisio(200,"platejat", "C", 12, 47)
    )
    var electroPB = 0
    var electroPF = 0
    var rentadoraPB = 0
    var rentadoraPF = 0
    var televisioPB = 0
    var televisioPF = 0
    for (i in 0..llistaElectro.lastIndex){
        println("Electrodomèstic ${i+1}:\n" +
                "Preu base: ${llistaElectro[i].preuBase}€\n" +
                "Color: ${llistaElectro[i].color}\n" +
                "Consum: ${llistaElectro[i].consum}\n" +
                "Pes: ${llistaElectro[i].pes}kg")
        llistaElectro[i].preuFinal()
        println("Preu final: ${llistaElectro[i].preuBase}€\n")
        electroPB += llistaElectro[i].preuBase
        electroPF += llistaElectro[i].preuFinal
        when(llistaElectro[i].javaClass) {
            Rentadora::class.java -> {
                rentadoraPB += llistaElectro[i].preuBase
                rentadoraPF += llistaElectro[i].preuFinal
            }
            Televisio::class.java -> {
                televisioPB += llistaElectro[i].preuBase
                televisioPF += llistaElectro[i].preuFinal
            }
        }
    }
    println("- Electrodomèstics:\n" +
            "   - Preu base: $electroPB€\n" +
            "   - Preu final: $electroPF€\n" +
            "- Rentadores:\n" +
            "   - Preu base: $rentadoraPB€\n" +
            "   - Preu final: $rentadoraPF€\n" +
            "- Televisions:\n" +
            "   - Preu base: $televisioPB€\n" +
            "   - Preu final: $televisioPF€\n")
}