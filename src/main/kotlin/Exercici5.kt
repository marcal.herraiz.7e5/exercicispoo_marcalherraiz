
class MechanicalArm(var openAngle: Double = 0.0, var altitude: Double= 0.0, var turnedOn: Boolean= false){
    fun toggle() {
        turnedOn = !turnedOn
        if (turnedOn){
            openAngle=0.0
            altitude= 0.0
        }
    }
    fun updateAngle(i: Int) {
        if (turnedOn){
            openAngle+=i.toDouble()
            if (0.0 > openAngle) { openAngle = 0.0 }
            else if (openAngle > 360.0) { openAngle = 360.0 }
        }
    }
    fun updateAltitude(i: Int) {
        if (turnedOn){
            altitude+=i.toDouble()
            if (0.0 > altitude) { altitude = 0.0 }
            else if (altitude > 30.0) { altitude = 30.0 }
        }
    }
}

fun main() {
    val mechanicalArm = MechanicalArm()
    mechanicalArm.toggle()
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAltitude(3)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAngle(180)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAltitude(-3)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAngle(-180)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAltitude(3)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.toggle()
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
}